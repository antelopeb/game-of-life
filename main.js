!function(){
    // make an iterator to build the array
    var width = 20,
        height = 30,
        grid = [],
        reset = document.getElementById("reset"),
        generation = document.getElementById("generation"),
        start = document.getElementById("start"),
        generationCount = 0,
        interval
    
    reset.addEventListener("click", function(e){
        e.preventDefault()
        clearInterval(interval)
        generationCount = 0
        buildGrid(width,height)
    })
  
    start.addEventListener("click", function(e){
        e.preventDefault()
        interval = setInterval(function(){
            checkAlive()
        },200)
    })
    
    generation.addEventListener("click", function(e){
        e.preventDefault()
        moveOne()
    })
    
    function buildGrid(w,h){        
        for(var i=0;i<w;i++){
        		// make the row first
        		grid[i] = []
            for(var ii=0;ii<h;ii++){
            	// make the cell and push it in
                var cell = {
                    alive : false,
                    x : i,
                    y : ii
                }
                grid[i].push(cell)
            }
        }
        
        var genMax = document.getElementById("max-gen")
        if(!localStorage.gameoflife){
        		localStorage.gameoflife = 0
        }
        genMax.innerHTML = localStorage.gameoflife

        makeGrid()
    }
    
    function makeGrid(){
        var gridContainer = document.getElementById("attachpoint"),
				genSpan = document.getElementById("gen-counter")
        gridContainer.innerHTML = ""
        genSpan.innerHTML = generationCount
        // build out objects
        for(var key in grid){
        		// make a row first, then fill with children
        		var row = grid[key],
        			 div = document.createElement("div")
        		div.className = "row"
        		for(var key2 in row){
        			var item = row[key2],
        				 cell = document.createElement("div")
        			cell.setAttribute("data-x", item.x)
        			cell.setAttribute("data-y", item.y)
        			cell.setAttribute("tabIndex", "0")
        			cell.className = item.alive ? "cell alive" : "cell dead"

        			cell.addEventListener("click", function(e){
        				e.preventDefault()
        				var x = e.target.getAttribute("data-x"),
        					 y = e.target.getAttribute("data-y"),
        					 newItem = grid[x][y]
        				if(e.target.className == "cell dead"){
                    newItem.alive = true
                	} else {
                    newItem.alive = false
                	}
                
                	makeGrid()
        			})
        			
        			div.appendChild(cell)
        		}
            
            gridContainer.appendChild(div)
        }
    }
    
    function moveOne(){
        var newGrid = JSON.parse(JSON.stringify(grid))
        // change one generation
        // a neighbor is a living cell
        // more than 3 neighbors, it dies
        // less than 2 neighbors, it dies
        // exactly 3 neighbors and dead, spring to life
        for(var key in grid){
        		var row = grid[key]
        		for(var key2 in row){
        			var oldcell = row[key2],
        				 newcell = newGrid[key][key2],
        				 siblings = getSiblings(oldcell)
					changeIt(oldcell, newcell, siblings)
        		}
        }

        grid = newGrid
        generationCount += 1
			// check to see if it's the highest count or not
			if(localStorage && localStorage.gameoflife){
				if(localStorage.gameoflife < generationCount){
					localStorage.gameoflife = generationCount
				}
			} else if(localStorage){
				localStorage.gameoflife = generationCount
			}
        
        makeGrid()
    }
    
    function changeIt(oldcell, newcell, siblings){
    		var alives = 0
 	      // count the alives
  			for(var key in siblings){
  				var sibling = siblings[key]
  				if(sibling.alive){
  					alives += 1
  				}
  			}

  			// apply the rules
  			if(alives > 3){
  				// die!
  				newcell.alive = false
  			}
  			if(alives < 2){
  				newcell.alive = false
  			}
  			if(alives === 3 && oldcell.alive === false){
  				newcell.alive = true
  			}
    }
    
    function checkAlive(){
        var alive = false
        for(var key in grid){
            var row = grid[key]
            for(var key2 in row){
            	var item = row[key2]
            	if(item.alive){
               	alive = true
            	}
            }

        }
        
        if(!alive){
            clearInterval(interval)
        } else {
            moveOne()
        }
    }
    
    function getSiblings(item){
        var siblings = [],
        		x = item.x,
        		y = item.y,
				toCheck = makeToCheck(item)
        
        // get the siblings by calculating them
        // x - 1, y
        // x - 1, y - 1
        // x - 1, y + 1
        
        // x + 1, y
        // x + 1, y - 1
        // x + 1, y + 1
        
        // x, y - 1
        // x, y + 1
        
        for(var key in toCheck){
        		var tc = toCheck[key].split(",")
        		if(isValid(tc[0],tc[1])){
        			siblings.push(grid[tc[0]][tc[1]])
        		}
        }

        return siblings
    }
    
    function makeToCheck(item){
    	var sendBack = []
    	
    	var x1 = item.x-1,
    		 x2 = item.x+1,
			 y1 = item.y-1,
			 y2 = item.y+1
    	sendBack.push(x1+","+item.y)
    	sendBack.push(x1+","+y1)
    	sendBack.push(x1+","+y2)
    	sendBack.push(x2+","+item.y)
    	sendBack.push(x2+","+y1)
    	sendBack.push(x2+","+y2)
    	sendBack.push(item.x+","+y1)
		sendBack.push(item.x+","+y2)
		    	
    	return sendBack
    }
    
    function isValid(x,y){
    		// is it a valid sibling?
    		// return t/f
    		var valid = false
    		
    		if(grid[x] && grid[x][y]){
    			valid = true
    		}
    		
    		return valid
    }
    
    buildGrid(width,height)
}()
